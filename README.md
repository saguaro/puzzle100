# puzzle100

Emulator to obtain a solution for puzzle 100 of the 100 door puzzle challenge. I thought this recursive algorithm was more fun to write than trying to find the solution with the app