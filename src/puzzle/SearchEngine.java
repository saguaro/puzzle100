package puzzle;

import java.util.*;
import java.util.stream.Collectors;

import static puzzle.Tile.wall;

public class SearchEngine {

    public static void main(String[] args) {

        Set<Tile> freePositions = getAllPositions();
        freePositions.removeAll(getOccupiedPositions());
        Board startBoard = new Board(freePositions);

        for (Tile start : freePositions) {
            Board board = new Board(startBoard);
            Tile localStart = board.findTile(start.x(), start.y()).orElseThrow(() -> new IllegalStateException("Position not found on board"));
            board.setPawn(localStart);
            List<String> path = Arrays.asList(String.format("Started at (%d,%d)", start.x(), start.y()));
            exploreAllDirectionsAndRecurse(board, path);
        }
        System.out.println("Engine completed");
    }

    private static Set<Tile> getOccupiedPositions() {
        return new HashSet<>(Arrays.asList(
                wall(2,0),
                wall(0,1),
                wall(1,1),
                wall(2,5),
                wall(4,8),
                wall(5,8),
                wall(6,8),
                wall(7,7)
        ));
    }

    private static Set<Tile> getAllPositions() {
        Set<Tile> freePositions = new HashSet<>();
        for (int y=0; y<=4; y++) {
            for (int x=0; x<=4+y; x++) {
                freePositions.add(Tile.freeTile(x,y));
            }
        }

        for (int start=1; start<=4; start++) {
            for (int x=start; x<=8; x++) {
                freePositions.add(Tile.freeTile(x,start+4));
            }
        }
        return freePositions;
    }

    private static void exploreAllDirectionsAndRecurse(Board board, List<String> path) {
        for (Direction direction : Direction.values()) {
            if (direction.from(board.pawn(), board).isFree()) {
                List<String> newPath = new ArrayList<>(path);
                newPath.add(""+direction);
                Board copy = new Board(board);
                copy.movePawn(direction);
                if (copy.isComplete()) {
                    System.out.print(path.stream().collect(Collectors.joining("  ")));
                    System.out.println("   We have a solution!!");
                }
                exploreAllDirectionsAndRecurse(copy, newPath);
            }
        }
    }
}
