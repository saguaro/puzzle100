package puzzle;

import java.util.Objects;

public final class Tile {
    private final int x,y;
    private boolean free;


    private Tile(int x, int y, boolean free) {
        this.x = x;
        this.y = y;
        this.free = free;
    }

    protected Tile(Tile original) {
        this.x = original.x;
        this.y = original.y;
        this.free = original.free;
    }

    public static Tile freeTile(int x, int y) {
        return new Tile(x,y,true);
    }

    public static Tile wall(int x, int y) {
        return new Tile(x,y,false);
    }

    public int x() {
        return x;
    }

    public int y() {
        return y;
    }

    public boolean isFree() {
        return free;
    }

    public void markOccupied() {
        this.free = false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tile that = (Tile) o;
        return x == that.x &&
                y == that.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

}
