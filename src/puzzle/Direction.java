package puzzle;

public enum Direction {
    NORTH_EAST(0,-1),
    EAST(1, 0),
    SOUTH_EAST(1,1),
    SOUTH_WEST(0,1),
    WEST(-1,0),
    NORTH_WEST(-1,-1);


    private final int deltaX;
    private final int deltaY;

    Direction(int deltaX, int deltaY) {
        this.deltaX = deltaX;
        this.deltaY = deltaY;
    }

    public Tile from(Tile start, Board board) {
        return board.findTile(start.x() + deltaX, start.y() + deltaY).orElse(Tile.wall(start.x() + deltaX, start.y() + deltaY));
    }
}
