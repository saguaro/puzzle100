package puzzle;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

public class Board {
    private final Set<Tile> tiles = new HashSet<>();
    private Tile pawn = null;

    public Board(Set<Tile> tileSet) {
        tileSet.forEach(c -> tiles.add(c));
    }

    public Board(Board original) {
        tiles.addAll(original.tiles.stream().map(Tile::new).collect(toSet()));
        pawn = original.pawn;
    }

    public void setPawn(Tile start) {
        if (!tiles.contains(start) && start.isFree()) {
            throw new IllegalStateException("Invalid position for Pawn: " + start);
        }
        start.markOccupied();
        pawn = start;
    }

    public Optional<Tile> findTile(int x, int y) {
        return tiles.stream()
                .filter(t -> t.x()==x)
                .filter(t -> t.y()==y)
                .findAny();
    }

    public Tile pawn() {
        return pawn;
    }

    public void movePawn(Direction direction) {
        Tile step = direction.from(pawn, this);
        while (step.isFree()) {
            step.markOccupied();
            pawn = step;
            step = direction.from(step, this);
        }
    }

    public boolean isComplete() {
        return tiles.stream().noneMatch(t -> t.isFree());
    }


}
